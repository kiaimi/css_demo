import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: '*',
    component: () => import('@/views/Home/index')
  },
  {
    path: '/shiny-glass',
    component: () => import('@/views/ShinyGlass/index')
  },
  {
    path: '/water-drop',
    component: () => import('@/views/WaterDrop/index')
  },
  {
    path: '/flash-card',
    component: () => import('@/views/FlashCard/index')
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
